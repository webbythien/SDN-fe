FROM node:21-alpine3.17

WORKDIR /webapps

COPY package.json .
RUN npm i --legacy-peer-deps
COPY . .
RUN npm run build

CMD ["npm", "start"]
